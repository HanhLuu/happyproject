from flask import Flask, render_template, flash, request, redirect
from rdflib import Graph
from rdflib.namespace import FOAF
from flask_wtf import FlaskForm
from tables import Results, resultObject
from sparql_endpoint import search_endpoint
from SPARQLWrapper import SPARQLWrapper, JSON
import json
import requests
#index.html and result.html are created here

app = Flask(__name__)
app.secret_key = 'not 4 you bro'

#create index.html with search mask
@app.route('/', methods=['GET', 'POST'])
def index():

    print(request.form)
    search = request.form
    print(type(search))
    print(search)
    print(request.method)
    if request.method == "POST":
        search_name = request.form['search_name']
        select_re = request.form['select_re']
        select_in = request.form['select_in']
        select_cl = request.form['select_cl']
        '''
        box_neighbour = request.form['neighbour']
        print("checkbox")
        print(box_neighbour)
        print(type(box_neighbour))
        '''
        search_count = request.form['search_count']
        print(search_name)
        print(select_re)
        print(select_in)
        print(select_cl)
        print(search_count)
        print("check request")
        return search_results(search)     
    return render_template('index.html')

#create result.html with table
@app.route('/results')
def search_results(search):
    '''
    checkbox = str 1 when checked
    checkbox = str 0 where not checked
    '''
    checkbox=''
    country_name = search['search_name']
    print(len(search))
    if len(search)==6:
        checkbox = search['neighbour']
    else:
        checkbox = str(0)
    print("my_checkbox: " + checkbox)
    print(country_name)
    region = search['select_re']
    income = search['select_in']
    climate = search['select_cl']
    count = search['search_count']
    print(region + " " + income + " " + " " + climate + " "+count)
    '''
    check box imolementierung
    '''
    results=search_endpoint(country_name,checkbox,region,income,climate,count)
    print("check")
    print(results) #list of dictionary
    for countryObj in results: #countryObj should be a dictionary
        print(countryObj.keys())
        print(countryObj.values())
            
    if not results: #results is empty
        print("checking for results if empty")
        flash('No results found!')
        print("no results")
        return redirect('/')
    else:
        '''
        self.name = name
        self.ranking = ranking
        self.region = region
        self.climate = climate
        self.income = income
        self.population = population
        self.life = life
        self.gdp = gdp
        self.internetuser = internetuser
        self.co2 = co2

        '''
        print("checking")
        results_table = []
        for countryObject in results:
            #countryObeject is a dictionray of a list results. Each country is a dictionary
            aObject = resultObject(countryObject['name'],countryObject['ranking'],countryObject['region'],
                                    countryObject['climate'],countryObject['income'],countryObject['pop'],
                                    countryObject['life'],countryObject['gdp'],countryObject['internetuser'],
                                    countryObject['co2'])
            results_table.append(aObject)
        my_table = Results(results_table)
        print(my_table)
        my_table.border = True
        return render_template('result.html', table=my_table)
if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000)
