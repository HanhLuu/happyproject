# -*- coding: utf-8 -*-
"""
Created on Mon Aug  5 08:21:03 2019

@author: GPRA489
"""
#defines our table
from flask_table import Table, Col
 
class Results(Table):
    id = Col('Id', show=False)
    name = Col('Name')
    ranking = Col('Ranking')
    region = Col('Region')
    climate = Col('Climate')
    income = Col('Income')
    population = Col('Population')
    life = Col('Life expectancy')
    gdp = Col('GDP')
    internetuser = Col('Internetuser')
    co2 = Col('CO2 emission')   
class resultObject(object):
    def __init__(self, name, ranking, region, climate, income, population, life, gdp, internetuser, co2):
        self.name = name
        self.ranking = ranking
        self.region = region
        self.climate = climate
        self.income = income
        self.population = population
        self.life = life
        self.gdp = gdp
        self.internetuser = internetuser
        self.co2 = co2


	
