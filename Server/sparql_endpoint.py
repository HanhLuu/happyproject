#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 19 23:14:29 2019

@author: miss-luu
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 18 20:45:21 2019

@author: miss-luu
"""
#this is the SPARQL-endpoint to make requests to database and filter the results
import copy
from SPARQLWrapper import SPARQLWrapper, JSON
import json
import requests

def search_endpoint(name,checkbox,region,income,climate,count):
    if count=='':
        count=50         #fix count = 50 when no input for max results
    else:
        count=int(count)
    endpoint = 'http://webengineering.ins.hs-anhalt.de:32164/happydb/query'   
    sparql = SPARQLWrapper(endpoint)
    sparql.setCredentials('happyproject', 'hanhangie')
    list_of_countries = []
    #===========================================================
    #alway find by name first
    if len(name)!=0:
        if checkbox==str(0):
            name = '"'+name+'"'
            rq = """
            
             PREFIX cc: <http://creativecommons.org/ns#> 
                PREFIX dcterms: <http://purl.org/dc/terms/> 
                PREFIX foaf: <http://xmlns.com/foaf/0.1/> 
                PREFIX gn: <http://www.geonames.org/ontology#> 
                PREFIX owl: <http://www.w3.org/2002/07/owl#>
                PREFIX dbpedia-owl:<http://dbpedia.org/ontology/>
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#> 
                PREFIX xml: <http://www.w3.org/XML/1998/namespace> 
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                PREFIX vrank: <http://purl.org/voc/vrank#>
                PREFIX wb: <http://data.worldbank.org/>
                PREFIX earth: <http://linked.earth/ontology#>
                PREFIX hto: <http://vcharpenay.github.io/hto/hto.xml#>
                PREFIX cro: <http://rhizomik.net/ontologies/copyrightonto.owl#>
            
            
                select * where {?s <http://www.geonames.org/ontology#name>""" + name + """ .
                           ?s <http://www.geonames.org/ontology#name> ?name .
                           ?re rdfs:label ?region .
						   ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#hasRegion> ?re .
                           ?in rdfs:label ?income .
						   ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#hasIncome> ?in .
                           ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#lifeExpentancy> ?life .
                           ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#GDP> ?gdp .
                           ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Internetuser> ?internetuser .
                           ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Population> ?pop .
                           ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Co2> ?co2 .
                           ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Rank> ?ranking .
                           ?cl rdfs:label ?climate .
						   ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#hasClimate> ?cl
                           }
                limit 10
            """
            sparql.setQuery(rq)
            sparql.setReturnFormat(JSON)
            

            
            data_json = sparql.query().convert()
            '''
            data_json["results"]["bindings"]: list of dictionary
            
            {income:}
            '''
            for result in data_json["results"]["bindings"]:
                countryObject={}
                for key, value in result.items():
                    #my_attribute = {}
                    #my_attribute[key] = value['value']
                    countryObject.update({key:value['value']})
                list_of_countries.append(countryObject)
            return list_of_countries
        else:
			#search for neighbours too
            name = '"'+name+'"'
            rq = """
            
             PREFIX cc: <http://creativecommons.org/ns#> 
                PREFIX dcterms: <http://purl.org/dc/terms/> 
                PREFIX foaf: <http://xmlns.com/foaf/0.1/> 
                PREFIX gn: <http://www.geonames.org/ontology#> 
                PREFIX owl: <http://www.w3.org/2002/07/owl#>
                PREFIX dbpedia-owl:<http://dbpedia.org/ontology/>
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#> 
                PREFIX xml: <http://www.w3.org/XML/1998/namespace> 
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                PREFIX vrank: <http://purl.org/voc/vrank#>
                PREFIX wb: <http://data.worldbank.org/>
                PREFIX earth: <http://linked.earth/ontology#>
                PREFIX hto: <http://vcharpenay.github.io/hto/hto.xml#>
                PREFIX cro: <http://rhizomik.net/ontologies/copyrightonto.owl#>
            
            
                select ?name ?region ?income ?life ?gdp ?internetuser ?pop ?co2 ?ranking ?climate where {
							{?s <http://www.geonames.org/ontology#name>""" + name + """ .
							?s <http://www.geonames.org/ontology#neighbour> ?nei .
							?nei <http://www.geonames.org/ontology#name> ?name .
							?nre rdfs:label ?region .
							?nei <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#hasRegion> ?nre .
							?nin rdfs:label ?income .
							?nei <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#hasIncome> ?nin .
							?nei <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#lifeExpentancy> ?life .
							?nei <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#GDP> ?gdp .
							?nei <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Internetuser> ?internetuser .
							?nei <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Population> ?pop .
							?nei <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Co2> ?co2 .
							?nei <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Rank> ?ranking .
							?ncl rdfs:label ?climate .
							?nei <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#hasClimate> ?ncl}
							UNION{
							?s <http://www.geonames.org/ontology#name>""" + name + """ .
                           ?s <http://www.geonames.org/ontology#name> ?name .
                           ?re rdfs:label ?region .
						   ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#hasRegion> ?re .
                           ?in rdfs:label ?income .
						   ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#hasIncome> ?in .
                           ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#lifeExpentancy> ?life .
                           ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#GDP> ?gdp .
                           ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Internetuser> ?internetuser .
                           ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Population> ?pop .
                           ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Co2> ?co2 .
                           ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Rank> ?ranking .
                           ?cl rdfs:label ?climate .
						   ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#hasClimate> ?cl
							}
							
							}
            """
            sparql.setQuery(rq)
            print("check neigbour")
            sparql.setReturnFormat(JSON)

            data_json = sparql.query().convert()
            '''
            data_json["results"]["bindings"]: list of dictionary
            
            {income:}
            '''
            for result in data_json["results"]["bindings"]:
                countryObject={}
                for key, value in result.items():
                    #my_attribute = {}
                    #my_attribute[key] = value['value']
                    countryObject.update({key:value['value']})
                list_of_countries.append(countryObject)
            return list_of_countries
        
    if len(name)==0:
        if region!="All":
            print("check request endpint 1")
            #request for specific region:
            region = '"' + region+'"'
            rq = """
            PREFIX cc: <http://creativecommons.org/ns#> 
            PREFIX dcterms: <http://purl.org/dc/terms/> 
            PREFIX foaf: <http://xmlns.com/foaf/0.1/> 
            PREFIX gn: <http://www.geonames.org/ontology#> 
            PREFIX owl: <http://www.w3.org/2002/07/owl#>
            PREFIX dbpedia-owl:<http://dbpedia.org/ontology/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
            PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#> 
            PREFIX xml: <http://www.w3.org/XML/1998/namespace> 
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            PREFIX vrank: <http://purl.org/voc/vrank#>
            PREFIX wb: <http://data.worldbank.org/>
            PREFIX earth: <http://linked.earth/ontology#>
            PREFIX hto: <http://vcharpenay.github.io/hto/hto.xml#>
            PREFIX cro: <http://rhizomik.net/ontologies/copyrightonto.owl#>
        
        
            select * where {?re rdfs:label """+region+""" .
                       ?re rdfs:label ?region .
					   ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#hasRegion> ?re .
                       ?s <http://www.geonames.org/ontology#name> ?name .
                       ?in rdfs:label ?income .
					   ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#hasIncome> ?in .
                       ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#lifeExpentancy> ?life .
                       ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#GDP> ?gdp .
                       ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Internetuser> ?internetuser .
                       ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Population> ?pop .
                       ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Co2> ?co2 .
                       ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Rank> ?ranking .
                       ?cl rdfs:label ?climate .
					   ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#hasClimate> ?cl
                       }
        """
            sparql.setQuery(rq)
            sparql.setReturnFormat(JSON)
            data_json = sparql.query().convert()
            '''
            data_json["results"]["bindings"]: list of dictionary
            
            {income:}
            '''
            print("check request endpoint")

            for result in data_json["results"]["bindings"]:
                print(result)
                countryObject={}
                for key, value in result.items():
                    countryObject.update({key:value['value']})
                list_of_countries.append(countryObject)#put all results for region in a list
            print("check again")
            print(len(list_of_countries))
            #check for income
            if income!="All":
                print("input income: " + income)
                i=0
                for countryObject in list_of_countries:
                    i = i+1
                print(i)
                list_countries_temp = copy.deepcopy(list_of_countries)
                print([(countryObject["name"],countryObject["income"]) for countryObject in list_countries_temp])
                
                for countryObject in list_countries_temp:
                    if countryObject['income']!=income:
                        list_of_countries.remove(countryObject)#remove all unwanted countrys from list by income
                    else:
                        print("h")
			#check for climate			
            if climate!="All":
                list_countries_temp = copy.deepcopy(list_of_countries)
                print([(countryObject["name"],countryObject["climate"]) for countryObject in list_countries_temp])
                
                for countryObject in list_countries_temp:
                    if countryObject['climate']!=climate:
                        list_of_countries.remove(countryObject)#remove all unwanted countrys from list by climate
                    else:
                        print("h")

            if len(list_of_countries) <= count:
                return list_of_countries
            else:
                return list_of_countries[:count]
        if region=="All":
            #request if region irrelevant:
            rq = """
            PREFIX cc: <http://creativecommons.org/ns#> 
            PREFIX dcterms: <http://purl.org/dc/terms/> 
            PREFIX foaf: <http://xmlns.com/foaf/0.1/> 
            PREFIX gn: <http://www.geonames.org/ontology#> 
            PREFIX owl: <http://www.w3.org/2002/07/owl#>
            PREFIX dbpedia-owl:<http://dbpedia.org/ontology/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
            PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#> 
            PREFIX xml: <http://www.w3.org/XML/1998/namespace> 
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            PREFIX vrank: <http://purl.org/voc/vrank#>
            PREFIX wb: <http://data.worldbank.org/>
            PREFIX earth: <http://linked.earth/ontology#>
            PREFIX hto: <http://vcharpenay.github.io/hto/hto.xml#>
            PREFIX cro: <http://rhizomik.net/ontologies/copyrightonto.owl#>
        
        
            select * where {
                        ?s <http://www.geonames.org/ontology#name> ?name .
                        ?re rdfs:label ?region .
						?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#hasRegion> ?re .
                        ?in rdfs:label ?income .
						?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#hasIncome> ?in .
                       ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#lifeExpentancy> ?life .
                       ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#GDP> ?gdp .
                       ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Internetuser> ?internetuser .
                       ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Population> ?pop .
                       ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Co2> ?co2 .
                       ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#Rank> ?ranking .
                       ?cl rdfs:label ?climate .
					   ?s <https://gitlab.com/HanhLuu/happyproject/raw/master/ontology/happydata.owl#hasClimate> ?cl
                       }
        """
            sparql.setQuery(rq)
            sparql.setReturnFormat(JSON)
            data_json = sparql.query().convert()
            '''
            data_json["results"]["bindings"]: list of dictionary
            
            {income:}
            '''
            print("check request endpoint")

            for result in data_json["results"]["bindings"]:
                print(result)
                countryObject={}
                for key, value in result.items():
                    countryObject.update({key:value['value']})
                list_of_countries.append(countryObject)#put all results in a list
            print("check again")
            print(len(list_of_countries))        
            #check for income
            if income!="All":
                print("input income: " + income)
                i=0
                for countryObject in list_of_countries:
                    i = i+1
                print(i)
                list_countries_temp = copy.deepcopy(list_of_countries)
                print([(countryObject["name"],countryObject["income"]) for countryObject in list_countries_temp])
                
                for countryObject in list_countries_temp:
                    if countryObject['income']!=income:
                        list_of_countries.remove(countryObject)#remove all unwanted countrys from list by income
                    else:
                        print("h")
			#check for climate
            if climate!="All":
                list_countries_temp = copy.deepcopy(list_of_countries)
                print([(countryObject["name"],countryObject["climate"]) for countryObject in list_countries_temp])
                
                for countryObject in list_countries_temp:
                    if countryObject['climate']!=climate:
                        list_of_countries.remove(countryObject)#remove all unwanted countrys from list by climate
                    else:
                        print("h")

            if len(list_of_countries) <= count:
                return list_of_countries
            else:
                return list_of_countries[:count]
            
         