<<<<<<< HEAD
## -*- coding: utf-8 -*-
#"""
#Created on Fri Aug  2 11:03:22 2019

#@author: GPRA489
#"""

=======
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  2 11:03:22 2019

@author: GPRA489
"""

# forms.py

'''
Search Form for filtering our music database's result
'''
 
from wtforms import Form, StringField, SelectField, PasswordField
from flask_wtf import FlaskForm
from wtforms.validators import InputRequired, DataRequired

class CountrySearchForm(Form):
	choices = [('Country', 'Country'),
               ('Album', 'Album'),
               ('Publisher', 'Publisher')]
	choices_re = [('', ''),
			   ('East Asia & Pacific', 'East Asia & Pacific'),
               ('Europe & Central Asia', 'Europe & Central Asia'),
			   ('Middle East & North Africa', 'Middle East & North Africa'),
			   ('North America', 'North America'),
			   ('South Asia', 'South Asia'),
			   ('Sub-Saharan Africa', 'Sub-Saharan Africa'),
               ('Latin America & Caribbean', 'Latin America & Caribbean')]
	choices_in = [('', ''),
			   ('High income', 'High income'),
               ('Low & middle income', 'Low & middle income'),
			   ('Low income', 'Low income'),
			   ('Lower middle income', 'Lower middle income'),
			   ('Middle income', 'Middle income'),
               ('Upper middle income', 'Upper middle income')]
	choices_cl = [('', ''),
			   ('Temperate zone', 'Temperate zone'),
               ('Mediterranean/subtropical zone', 'Mediterranean/subtropical zone'),
			   ('Hot dry zone', 'Hot dry zone'),
			   ('Hot humid/tropical zone', 'Hot humid/tropical zone'),
               ('Hot/higher humidity', 'Hot/higher humidity')]
	select_re = SelectField('', choices=choices_re, validators=[DataRequired()])
	select_in = SelectField('', choices=choices_in,validators=[DataRequired()])
	select_cl = SelectField('', choices=choices_cl,validators=[DataRequired()])
	search_name = StringField('',validators=[DataRequired()])
	search_count = StringField('',validators=[DataRequired()])



'''
defines all the fields we need to creat a new Album
AlbumForm to make new instance for Album and then adding to database 
'''


	
>>>>>>> f4f86accb43612dba495c3dc4cc9b6f3d84ca2b4
