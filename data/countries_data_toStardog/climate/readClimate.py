#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 16 19:00:35 2019

@author: miss-luu
"""
#dictionary of type of climates
import pandas as pd
def readClimate_fromCSV():
    climate_type = pd.read_csv("climate/climate_zone.csv")
    climate_dict = {climate_type.iloc[i]["Zone"]:climate_type.iloc[i]["Type of climate"] for i in range(0,climate_type.shape[0])}
    #read table for countries and climates
    countries_climate = pd.read_csv("climate/country_climate.csv")
    countries_climate["Climate zone"].replace({"Iva":"IVa","Ivb":"IVb"},inplace=True)
    #replace climate Code to String with climate_dict
    countries_climate["Climate zone"].replace(climate_dict, inplace=True)
    countries_climate = countries_climate[["Country Name","Country Code","Climate zone"]]
    return countries_climate
#country_and_climate = readClimate_fromCSV()