from SPARQLWrapper import SPARQLWrapper

insertEndPoint = 'http://localhost:5820/test_data_end_2/update' #name of database
sparqlInsertEndPoint = SPARQLWrapper(insertEndPoint)
sparqlInsertEndPoint.setCredentials('hanh', 'hanh')
sparqlInsertEndPoint.setMethod("POST")

def insertCountry(mySubject, relation, myObject):
    
    #earth for hasLink
    
    print("TEST")
    print(mySubject)
    print(relation)
    print(myObject)
    insertQuery = """
    PREFIX cc: <http://creativecommons.org/ns#> 
    PREFIX dcterms: <http://purl.org/dc/terms/> 
    PREFIX foaf: <http://xmlns.com/foaf/0.1/> 
    PREFIX gn: <http://www.geonames.org/ontology#> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX dbpedia-owl:<http://dbpedia.org/ontology/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#> 
    PREFIX xml: <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX vrank: <http://purl.org/voc/vrank#>
    PREFIX wb: <http://data.worldbank.org/>
    PREFIX earth: <http://linked.earth/ontology#>
    PREFIX hto: <http://vcharpenay.github.io/hto/hto.xml#>
    PREFIX cro: <http://rhizomik.net/ontologies/copyrightonto.owl#>
    
    
    INSERT DATA {""" + mySubject + " " + relation + " " + myObject +""" .}"""
    
    print(insertQuery)
    sparqlInsertEndPoint.setQuery(insertQuery)
    sparqlInsertEndPoint.query()