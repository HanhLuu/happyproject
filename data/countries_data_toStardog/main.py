#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  9 01:55:07 2019

@author: miss-luu
"""
import rdflib
from SPARQLWrapper import SPARQLWrapper, JSON
import insertDatabase
import pandas as pd
import numpy as np
import world_bank_data as wb
from climate.readClimate import readClimate_fromCSV

'''
for neighbours data i use sparql Endpoint FF to get neigbouringFeatures from geonames.
neigbouringFeatures = links to neighbour countries
get neighbours for countries, which i have on my happiness ranking data
'''
def countryAndNeighbourRDF(country_names):
    sparql = SPARQLWrapper(" http://factforge.net/repositories/ff-news")
    countries = country_names
    list_results = {} #save country_name and link to their neighbours
    i = 0
    for name in countries:
        print(name)
        name = '"' + name + '"'
        query = """
        PREFIX gn: <http://www.geonames.org/ontology#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX foaf: <http://xmlns.com/foaf/0.1/>
        select * where {
            ?s foaf:name"""+name+"""@en.
            ?s gn:neighbouringFeatures  ?nei.
            #?n gn:Feature  ?nei
        } limit 10
        """
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        for result in results["results"]["bindings"]:
            if len(result)==0:
                print("not found")
                i = i + 1
            else:
                list_results[name]=result["nei"]["value"]
    return list_results
'''

'''
def country_link_name_neighbour_ranking_income_region(country_from_RankingData,list_results):
    relation_name = "gn:name"
    relation_neighbour = "gn:neighbour"
    relation_ranking_1 = "dbpedia-owl:countryRank"
    #relation_hasLink = "earth:hasLink"
    relation_region = "wb:region"
    relation_income = "wb:incomeLevel"
    i=0
    k=0
    for countryNameAsNeighbour, neighbourRDF in list_results.items():
        print("link to neighbour rdf: ")
        print(neighbourRDF)
        myGraph=rdflib.Graph()
        myGraph.load(neighbourRDF)
        print(myGraph)
        for s,p,o in myGraph:
            print(p)
            if (str(p) == "http://www.geonames.org/ontology#name"):
                print("subject")
                print(s)
                print("predikate")
                print(p)
                print("objects")
                print(o)
                
                countryEndpoint = "<"+str(s)+">"
                countryName = '"'+str(o)+'"'
                countryName_string=str(o)
                print(countryName_string)
                #insertDatabase.insertCountry(countryName_, relation_hasLink, countryEndpoint)
                insertDatabase.insertCountry(countryEndpoint,relation_name,countryName)
                '''
                insert data for income and region for a country, which have a name
                '''
                wb_data = wb.search_countries(countryName_string)
                if wb_data.shape[0]!=0:
                    region = '"' + str(wb_data["region"].iloc[0])+'"'
                    income = '"' + str(wb_data["incomeLevel"].iloc[0])+'"'
                    insertDatabase.insertCountry(countryEndpoint, relation_region, region )
                    insertDatabase.insertCountry(countryEndpoint, relation_income, income)
                else:
                    insertDatabase.insertCountry(countryEndpoint, relation_region, '"not given"')
                    insertDatabase.insertCountry(countryEndpoint, relation_income, '"not given"')
                    print("no given")
                
                if str(o) in list(country_from_RankingData["Country"]):
                    i = i +1 #countryName
                    getIndex =list(np.where(country_from_RankingData["Country"]==str(o))[0])[0]
                    print(getIndex)
                    Ranking = country_from_RankingData["Happiness.Rank"].iloc[getIndex]
                    countryRanking = str(Ranking)
                    insertDatabase.insertCountry(countryEndpoint,relation_ranking_1, countryRanking)
                else:
                    insertDatabase.insertCountry(countryEndpoint,relation_ranking_1, '"not given"')
                    print("not given to check")

            k = k +1
            if (str(p)=="http://www.geonames.org/ontology#neighbour"):
                print("subject")
                print(s)
                print("predikate")
                print(p)
                print("objects")
                print(o)
                countryEndpoint = "<"+str(s)+">"
                neighbourEndpoint = "<"+str(o)+">"
                
                insertDatabase.insertCountry(countryEndpoint,relation_neighbour,neighbourEndpoint)
                insertDatabase.insertCountry(neighbourEndpoint,relation_name, countryNameAsNeighbour)
def country_climate_insert(country_climate,list_results):
    relation_climate = "dbpedia-owl:climate"
    for countryNameAsNeighbour, neighbourRDF in list_results.items():
        print("link to neighbour rdf: ")
        print(neighbourRDF)
        myGraph=rdflib.Graph()
        myGraph.load(neighbourRDF)
        print(myGraph)
        for s,p,o in myGraph:
            print(p)
            if (str(p) == "http://www.geonames.org/ontology#name"):
                countryEndpoint = "<"+str(s)+">"
                #countryName = '"'+str(o)+'"'
                countryName_string=str(o)
                print(countryName_string)
                
                if str(o) in list(country_climate["Country Name"]): 
                    getIndex =list(np.where(country_climate["Country Name"]==str(o))[0])[0]
                    print(getIndex)
                    climate = country_climate["Climate zone"].iloc[getIndex]
                    climate = '"'+str(climate)+'"'
                    print(climate)
                    insertDatabase.insertCountry(countryEndpoint,relation_climate, climate)
                else:
                    insertDatabase.insertCountry(countryEndpoint,relation_climate, '"no data"')
                    print("not given to check")
def gdp_internet_life_pop(gdp, internet, life, pop, list_results):
    #
    relation_gdp = "dbpedia-owl:grossDomesticProduct"
    relation_int = "cro:count"
    relation_life = "dbpedia-owl:lifeExpectancy"
    relation_pop = "gn:population"
    rel_list = {relation_gdp:gdp,relation_int:internet,relation_life:life,relation_pop:pop}
    for countryNameAsNeighbour, neighbourRDF in list_results.items():
        print("link to neighbour rdf: ")
        print(neighbourRDF)
        myGraph=rdflib.Graph()
        myGraph.load(neighbourRDF)
        print(myGraph)
        for s,p,o in myGraph:
            print(p)
            if (str(p) == "http://www.geonames.org/ontology#name"):
                countryEndpoint = "<"+str(s)+">"
                #countryName = '"'+str(o)+'"'
                print(str(o))
                if str(o) in list(gdp["Country Name"]):
                    for rel,df in rel_list.items():
                        print(rel)
                        getIndex =list(np.where(df["Country Name"]==str(o))[0])[0]
                        print(getIndex)
                        value = df["2017 [YR2017]"].iloc[getIndex]
                        print(value)
                        value = '"'+str(value)+'"'
                        print(value)
                        insertDatabase.insertCountry(countryEndpoint,rel, value)
                else:
                    for rel,df in rel_list.items():
                        insertDatabase.insertCountry(countryEndpoint,rel, '"no data"')
                        print("not given to check")
    
def co_insert(co, list_results):
    relation_co = "hto:CO2"
    for countryNameAsNeighbour, neighbourRDF in list_results.items():
        print("link to neighbour rdf: ")
        print(neighbourRDF)
        myGraph=rdflib.Graph()
        myGraph.load(neighbourRDF)
        print(myGraph)
        for s,p,o in myGraph:
            print(p)
            if (str(p) == "http://www.geonames.org/ontology#name"):
                countryEndpoint = "<"+str(s)+">"
                #countryName = '"'+str(o)+'"'
                
                if str(o) in list(co["Country Name"]):
                    getIndex =list(np.where(co["Country Name"]==str(o))[0])[0]
                    print(getIndex)
                    value = co["2014 [YR2014]"].iloc[getIndex]
                    value = '"'+str(value)+'"'
                    print(value)
                    insertDatabase.insertCountry(countryEndpoint,relation_co, value)
                else:
                    insertDatabase.insertCountry(countryEndpoint,relation_co, '"no data"')
                    print("not given to check")
    
    
if __name__=='__main__':
    myHappyDataKaggle = pd.read_csv("happiness_ranking/2017.csv")
    country_names = myHappyDataKaggle["Country"].tolist()
    
    print(myHappyDataKaggle.columns)
    #list of country_name and link neighbour.rdf of this country_name
    list_results = countryAndNeighbourRDF(country_names)
    print("TEST")
    #get countries from World Bank Data
    wb_mycountries= wb.get_countries()
    wb_name_region_income = wb_mycountries[["name","region", "incomeLevel"]]
    #insert data to datase
    country_link_name_neighbour_ranking_income_region(myHappyDataKaggle, list_results)
    #get climate
    country_climate = readClimate_fromCSV()[["Country Name", "Climate zone"]]
    country_climate_insert(country_climate,list_results)
    #get world bank data
    co = pd.read_csv("world_bank_data/co2.csv")[["Country Name", "2014 [YR2014]"]]
    co_insert(co, list_results)
    #gdp.....
    gdp = pd.read_csv("world_bank_data/GDP.csv")[["Country Name", "2017 [YR2017]"]]
    internet = pd.read_csv("world_bank_data/internetuser.csv")[["Country Name", "2017 [YR2017]"]]
    life = pd.read_csv("world_bank_data/lifeExpectancy.csv")[["Country Name", "2017 [YR2017]"]]
    pop = pd.read_csv("world_bank_data/population.csv")[["Country Name", "2017 [YR2017]"]]
    gdp_internet_life_pop(gdp,internet,life,pop,list_results)