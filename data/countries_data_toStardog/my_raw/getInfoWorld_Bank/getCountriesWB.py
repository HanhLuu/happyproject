#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 15 11:53:28 2019

@author: miss-luu
"""
import pandas as pd
import world_bank_data as wb
country_code = wb.get_countries().name
#country_code[code] = name_country
#country_code.keys(): all code of countries
lE_indicator = wb.search_indicators("Life Expectancy") #life expectancy total
lE_indicator_code = lE_indicator.iloc[5].name #5: life expectancy total
#lE_indicator_code = 'SP.DYN.LE00.IN'
lifeEx = wb.get_series('SP.DYN.LE00.IN', mrv=1, gapfill='Y',date='2017', id_or_value='id', simplify_index=True)
#lifeEx[code] = age

gdp = wb.get_series("NY.GDP.MKTP.CD",mrv=1, gapfill='Y',date='2017', id_or_value='id', simplify_index=True)
new_df = pd.DataFrame()
new_df["code"] = gdb.keys()
new_df["gdp"] = gdb.values
