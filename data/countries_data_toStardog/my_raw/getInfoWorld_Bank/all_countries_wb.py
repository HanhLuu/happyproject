#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 15 00:03:30 2019

@author: miss-luu
"""

import rdflib
#from FF_endpoint import list_results
from SPARQLWrapper import SPARQLWrapper, JSON
import insertDatabase
import pandas as pd
import numpy as np
import requests


def allCountries(country_names):
    #from all_countries import country_names

    sparql = SPARQLWrapper("http://worldbank.270a.info/sparql")
            #"http://api.worldbank.org/v2/country")
    #"http://worldbank.270a.info/sparql")
            #"http://api.worldbank.org/v2/country")
    countries = country_names
    list_results = {}
    i = 0
    for name in countries:
        print(name)
        name = '"' + name + '"'
        query = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX wb: <http://data.worldbank.org/>
        select * 
        from <http://api.worldbank.org/v2/country> where {
            ?s wb:name"""+name+""".
            ?s wb:region  ?region.
            ?s wb:incomeLevel ?income
        } limit 10
        """
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        for result in results["results"]["bindings"]:
            if len(result)==0:
                print("not found")
                i = i + 1
            else:
                list_results[name]=result["nei"]["value"]
    return list_results

myHappyDataKaggle = pd.read_csv("2017.csv")
country_names = myHappyDataKaggle["Country"].tolist()
i = 0
g = rdflib.Graph()
g.load("http://api.worldbank.org/v2/country/")
for s,p,o in g:
    print(s,p,o)
    i = i +1