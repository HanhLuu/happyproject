#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 15 12:31:02 2019

@author: miss-luu
"""

import urllib3

# The URI for the data you want to fetch
http = urllib3.PoolManager()
uri = 'http://api.worldbank.org/v2/country/'
r = http.request('GET', uri)
print(r.data)

# The content type you want to set in the Request Headers.

# This example is for RDF/XML

request_headers = {'Accept': 'application/rdf+xml'}

# Build the request with the URI and Header parameters 

request = urllib3.request(uri, headers = request_headers)

# Fetch the request

response = urllib3.urlopen(request)

# Read and Print the request

data = response.read()

print(data)
#import urllib3
#>>> http = urllib3.PoolManager()
#>>> r = http.request('GET', 'http://httpbin.org/robots.txt')
#>>> r.status
#200
#>>> r.data