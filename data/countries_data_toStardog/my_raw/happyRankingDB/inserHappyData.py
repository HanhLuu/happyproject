#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 13 13:12:14 2019

@author: miss-luu
"""

from SPARQLWrapper import SPARQLWrapper, JSON
#from multiprocessing.dummy import Pool as ThreadPool
#import json
#import requests

insertEndPoint = 'http://localhost:5820/happyCountries/update'
sparqlInsertEndPoint = SPARQLWrapper(insertEndPoint)
sparqlInsertEndPoint.setCredentials('hanh', 'hanh')
sparqlInsertEndPoint.setMethod("POST")

def insertHappyCountry(mySubject, relation, myObject):
    
    #relation_name = "gn:name"
    #relation_neighbour = "gn:neighbour"
    #relation = "vrank:RankValue"
    print("TEST")
    print(mySubject)
    print(relation)
    print(myObject)
    insertQuery = """
    PREFIX cc: <http://creativecommons.org/ns#> 
    PREFIX dcterms: <http://purl.org/dc/terms/> 
    PREFIX foaf: <http://xmlns.com/foaf/0.1/> 
    PREFIX gn: <http://www.geonames.org/ontology#> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#> 
    PREFIX xml: <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX vrank: <http://purl.org/voc/vrank#>
    
    INSERT DATA {""" + mySubject + " " + relation + " " + myObject +""" .}"""
    
    print(insertQuery)
    sparqlInsertEndPoint.setQuery(insertQuery)
    sparqlInsertEndPoint.query()