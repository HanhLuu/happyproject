#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 13 14:52:31 2019

@author: miss-luu
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 13 13:12:14 2019

@author: miss-luu
"""

from SPARQLWrapper import SPARQLWrapper, JSON
import pandas as pd
from FF_endpoint import list_results
import rdflib
import numpy as np

insertEndPoint = 'http://localhost:5820/testOWL/update'
sparqlInsertEndPoint = SPARQLWrapper(insertEndPoint)
sparqlInsertEndPoint.setCredentials('hanh', 'hanh')
sparqlInsertEndPoint.setMethod("POST")

def insertHappyCountry(mySubject, relation, myObject):
    #relation = "vrank:RankValue"
    #relation = "dbpedia-owl:countryRank"
    print("TEST")
    print(mySubject)
    print(relation)
    print(myObject)
    insertQuery = """
    PREFIX cc: <http://creativecommons.org/ns#> 
    PREFIX dcterms: <http://purl.org/dc/terms/> 
    PREFIX foaf: <http://xmlns.com/foaf/0.1/> 
    PREFIX gn: <http://www.geonames.org/ontology#> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    PREFIX dbpedia-owl:<http://dbpedia.org/ontology/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#> 
    PREFIX xml: <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX vrank: <http://purl.org/voc/vrank#>
    
    
    INSERT DATA {""" + mySubject + " " + relation + " " + myObject +""" .}"""
    
    print(insertQuery)
    sparqlInsertEndPoint.setQuery(insertQuery)
    sparqlInsertEndPoint.query()
if __name__=='__main__':
    
    myCountries = pd.read_csv("2017.csv")
    country_names = myCountries["Country"].tolist()
    
    relation_ranking_1 = "dbpedia-owl:countryRank"
    relation_ranking_2 = "vrank:hasRank"
    relation_name="gn:name"
    #insertHappyCountry("<http://sws.geonames.org/2017370/>","vrank:hasRank", "49")
    
    for countryNameAsNeighbour, neighbourRDF in list_results.items():
        print("link to neighbour rdf: ")
        print(neighbourRDF)
        myGraph=rdflib.Graph()
        myGraph.load(neighbourRDF)
        print(myGraph)
        for s,p,o in myGraph:
            print(p)
            if (str(p) == "http://www.geonames.org/ontology#name"):
                print("subject")
                print(s) #s is a country
                print("predikate")
                print(p) #gn:name
                print("objects")
                print(o) #name of a country
                countryEndpoint = "<"+str(s)+">"
                countryName = '"'+str(o)+'"'
                print(countryName)
                if str(o) in list(myCountries["Country"]): #countryName
                    getIndex =list(np.where(myCountries["Country"]==str(o))[0])[0]
                    print(getIndex)
                    Ranking = myCountries["Happiness.Rank"].iloc[getIndex]
                    print(Ranking)
                    countryRanking = str(Ranking)
                    insertHappyCountry(countryEndpoint,relation_ranking_1, countryRanking)
                    insertHappyCountry(countryEndpoint,relation_name, countryName)
        myGraph.serialize(destination='output_with_owl.rdf', format='turtle')
    
    
    
    
    