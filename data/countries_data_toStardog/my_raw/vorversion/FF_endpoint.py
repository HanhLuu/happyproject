#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  8 22:58:03 2019

@author: miss-luu
"""


'''
find name and take the about.rdf, in order to take neighbour for next function
'''
from SPARQLWrapper import SPARQLWrapper, JSON

from all_countries import country_names

sparql = SPARQLWrapper(" http://factforge.net/repositories/ff-news")
countries = country_names
list_results = {}
i = 0
for name in countries:
    print(name)
    name = '"' + name + '"'
    query = """
    PREFIX gn: <http://www.geonames.org/ontology#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    select * where {
        ?s foaf:name"""+name+"""@en.
        ?s gn:neighbouringFeatures  ?nei.
        #?n gn:Feature  ?nei
    } limit 10
    """
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    for result in results["results"]["bindings"]:
        if len(result)==0:
            print("not found")
            i = i + 1
        else:
            list_results[name]=result["nei"]["value"]
print(list_results)