#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 18 20:45:21 2019

@author: miss-luu
"""

from SPARQLWrapper import SPARQLWrapper, JSON
import json
import requests

endpoint = 'http://webengineering.ins.hs-anhalt.de:32164/my_database/query'

sparql = SPARQLWrapper(endpoint)

# add your username and password if required
sparql.setCredentials('happyproject', 'hanhangie')

rq = """

 PREFIX cc: <http://creativecommons.org/ns#> 
    PREFIX dcterms: <http://purl.org/dc/terms/> 
    PREFIX foaf: <http://xmlns.com/foaf/0.1/> 
    PREFIX gn: <http://www.geonames.org/ontology#> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX dbpedia-owl:<http://dbpedia.org/ontology/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#> 
    PREFIX xml: <http://www.w3.org/XML/1998/namespace> 
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX vrank: <http://purl.org/voc/vrank#>
    PREFIX wb: <http://data.worldbank.org/>
    PREFIX earth: <http://linked.earth/ontology#>
    PREFIX hto: <http://vcharpenay.github.io/hto/hto.xml#>
    PREFIX cro: <http://rhizomik.net/ontologies/copyrightonto.owl#>


    select * where {?s <http://www.geonames.org/ontology#name> "Norway" .
               ?s <http://www.geonames.org/ontology#neighbour> ?nb .
               ?nb <http://www.geonames.org/ontology#name> ?name .
               ?nb <http://data.worldbank.org/region> ?region .
               ?nb <http://data.worldbank.org/incomeLevel> ?income .
               ?nb <http://dbpedia.org/ontology/lifeExpectancy> ?life .
               ?nb <http://dbpedia.org/ontology/grossDomesticProduct> ?gdp .
               ?nb <http://rhizomik.net/ontologies/copyrightonto.owl#count> ?internetuser .
               ?nb <http://www.geonames.org/ontology#population> ?pop .
               ?nb <http://vcharpenay.github.io/hto/hto.xml#CO2> ?co2 .
               ?nb <http://dbpedia.org/ontology/countryRank> ?ranking
               }
"""

sparql.setQuery(rq)
sparql.setReturnFormat(JSON)

# use reasoning
sparql.addParameter('reasoning', 'true')

data_json = sparql.query().convert()

for result in data_json["results"]["bindings"]:
    print(result)
    print(result["name"]["value"])