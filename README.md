# Happyproject

##Discription
In our project we created a webapp, which can be used to browse data about countries by name or properties. This data can be used to consider what circumstances are making people happy. 
A running version of this app you can find here:
```
http://webengineering.ins.hs-anhalt.de:32167
```

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Here is a list of things you need to install on your local machine:

- python
- Flask for Python
- Flask WTF
- WTForms
- Flask Table
- rdflib
- SPARQLWrapper
- json
- requests


### Installing

Here is a step by step series of examples that tell you how to get a development env running:


#### Install virtual environment
First, you will need to download Python, if you haven't already installed it. 
You can download it [here.](https://www.python.org/downloads/)

If you installed Python, we can go on with the virtual environment.
We will use virtualenv. You can install it with the command:
```
pip install virtualenv
```

We create a virtual environment by using the following command: 
```
virtualenv NameOfEnvironment
```

Now switch the directory:
```
cd NameOfEnvironment
```

To activate your virtual environment use:
For Linux:
```
source bin/activate
```
For Windows: 
```
Scripts/activate
```

Now you should see something like that in your terminal: 
```
(NameOfEnvironment) C:\Users\User\NameOfEnvironment>
```
Our virtual environment is running and we can start to install our packages.


#### Install packages on virtual environment
Now run the following installs in the virtual environment:
```
pip install flask
```
```
pip install Flask-WTF
```
```
pip install wtforms
```
```
pip install flask_table
```
```
pip install SPARQLWrapper
```
```
pip install rdflib
```
```
pip install json
```
```
pip install requests
```

Now you have installed all required packages. Time to pull the app. 
If you have already cloned the Project somewhere, you can copy the happyproject-folder into the NameOfEnvironment-Folder. Otherwise clone it by using:
```
git clone https://gitlab.com/HanhLuu/happyproject.git
```


## Running the app

Time to run the app. Go into the terminal and make sure the virtual environment is active. 
If not activate it like above.
Use the cd command to go into the directory happyproject/Server.
Run the main.py of the app with the following command: 
```
python main.py
```
Then something like that should appear:
```
 * Serving Flask app "main" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
```
If you see that in your terminal you can open a browser of your choice and enter the ip 127.0.0.1:5000 into the adress line. 
Now the app should be usable for you. 


If you want to stop the app, press ctrl+C in the terminal. 
If you want to deactivate the virtual environment go into the NameOfEnvironment-Directory and use:

For Linux:
```
source bin/deactivate
```
For Windows: 
```
Scripts/deactivate
```

## Notes about usage
If you managed to run the app and entered the website on localhost, you should see the user interface. 
Take a look into our [usermanual.md](https://gitlab.com/HanhLuu/happyproject/blob/master/usermanual.md) for more information. 
There are still some data missing in the database, so not all countries can be shown. 



## Built With
* [Flask](https://flask.palletsprojects.com/en/1.1.x/) - The web framework
* [Python](https://www.python.org/doc/) - The programming language
* [Stardog](https://www.stardog.com/docs/) - The Database


## Authors
- Thi Kim Hanh Luu
- Angie Gerbig 

## License 
This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/HanhLuu/happyproject/blob/master/LICENSE.md) file for details.


## Acknowledgments
- this project was part of the module "Linked Data and Semantic Web" at [Anhalt University of Applied Sciences](https://www.hs-anhalt.de/startseite.html)



