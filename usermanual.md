# User manual

## Discription
In our project we created a webapp, which can be used to browse data about countries by name or properties. This data can be used to consider what circumstances are making people happy. 
A running version of this app you can find here:
```
http://webengineering.ins.hs-anhalt.de:32167
```

## How to use the search-mask
There are two ways of searching with the help of the search-mask.
The search-mask is subdivided (by their orange borders) in an upper and a lower search-section.
In the upper search-section you can search countries by name. 
In the lower search-section you can search countries by specific properties
If you enter a name in the upper search section, the lower section will be ignored.
If you don't enter a name in the upper section, the upper section will be ignored. 

### Using the upper search-section
If you decide to search for a country by name, enter the English name of that country into the search field named "Name of country:". Please notice that the first letter always must be a capital!
If you like to search for the neighbouring countries too, turn on the switch-button named "Also look for happiness of neighboring countries". 
If you are ready, submit your request by clicking the "Search"-button. 

### Using the lower search-section
If you have not entered a name, the lower search-section will be used. 
In the lower search-section you have three properties to choose.
These properties are "Region", "Income" and "Climate".
The preset value of all three properties is "All". If you don't change it and click on search the results will show the first 50 results from the database, because the maximum count of result is preset by 50.
You can change the values of the properties by clicking the choice-field and select a value. 
If you want to change the maximum count of results, enter an integer into the "Maximum count of results:"-field. 
If you are ready, submit your request by clicking the "Search"-button.


## Results

### A result was found in database
If the search was successful the results will be displayed in a table. 
The table has following values:
- Name (of country)
- Ranking (Happinessranking, place)
- Region (the country is located in)
- Climate (zone)
- Income (of country)
- Population (of country)
- Life Expectancy (of people in the country)
- GDP (gross domestic product per capita)
- Internetuser (% of population using internet)
- CO2 emission (metric tons per capita, per year)


### No result was found in the database
If there are no matching results in the database, a flash-message appears right next to the search-button, which says "No results found!". You can go on searching with the help of the search-mask. 

