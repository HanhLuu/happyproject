FROM python:3.7.3
RUN pip install --upgrade pip --user
RUN useradd -ms /bin/bash admin
USER admin
WORKDIR /Server
COPY Server /Server
RUN pip install --trusted-host pypi.python.org -r requirements.txt --user
ENV myPythonServer World
CMD ["python", "main.py"]
