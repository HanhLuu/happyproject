#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 19 23:14:29 2019

@author: miss-luu
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 18 20:45:21 2019

@author: miss-luu
"""
import copy
from SPARQLWrapper import SPARQLWrapper, JSON
import json
import requests

def search_endpoint(name,checkbox,region,income,climate,count):
    if count=='':
        count=50         #fix count = 50 when no input for max results
    else:
        count=int(count)
    endpoint = 'http://webengineering.ins.hs-anhalt.de:32164/my_database/query'   
    sparql = SPARQLWrapper(endpoint)
    sparql.setCredentials('happyproject', 'hanhangie')
    list_of_countries = []
    #===========================================================
    #alway find by name first
    if len(name)!=0:
        if checkbox==str(0):
            name = '"'+name+'"'
            rq = """
            
             PREFIX cc: <http://creativecommons.org/ns#> 
                PREFIX dcterms: <http://purl.org/dc/terms/> 
                PREFIX foaf: <http://xmlns.com/foaf/0.1/> 
                PREFIX gn: <http://www.geonames.org/ontology#> 
                PREFIX owl: <http://www.w3.org/2002/07/owl#>
                PREFIX dbpedia-owl:<http://dbpedia.org/ontology/>
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#> 
                PREFIX xml: <http://www.w3.org/XML/1998/namespace> 
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                PREFIX vrank: <http://purl.org/voc/vrank#>
                PREFIX wb: <http://data.worldbank.org/>
                PREFIX earth: <http://linked.earth/ontology#>
                PREFIX hto: <http://vcharpenay.github.io/hto/hto.xml#>
                PREFIX cro: <http://rhizomik.net/ontologies/copyrightonto.owl#>
            
            
                select * where {?s <http://www.geonames.org/ontology#name>""" + name + """ .
                           ?s <http://www.geonames.org/ontology#name> ?name .
                           ?s <http://data.worldbank.org/region> ?region .
                           ?s <http://data.worldbank.org/incomeLevel> ?income .
                           ?s <http://dbpedia.org/ontology/lifeExpectancy> ?life .
                           ?s <http://dbpedia.org/ontology/grossDomesticProduct> ?gdp .
                           ?s <http://rhizomik.net/ontologies/copyrightonto.owl#count> ?internetuser .
                           ?s <http://www.geonames.org/ontology#population> ?pop .
                           ?s <http://vcharpenay.github.io/hto/hto.xml#CO2> ?co2 .
                           ?s <http://dbpedia.org/ontology/countryRank> ?ranking .
                           ?s <http://dbpedia.org/ontology/climate> ?climate
                           }
                limit 10
            """
            sparql.setQuery(rq)
            sparql.setReturnFormat(JSON)
            
            #sparql.addParameter('reasoning', 'true')
            
            data_json = sparql.query().convert()
            '''
            data_json["results"]["bindings"]: list of dictionary
            
            {income:}
            '''
            for result in data_json["results"]["bindings"]:
                countryObject={}
                for key, value in result.items():
                    #my_attribute = {}
                    #my_attribute[key] = value['value']
                    countryObject.update({key:value['value']})
                list_of_countries.append(countryObject)
            return list_of_countries
        else:
            name = '"'+name+'"'
            rq = """
            
             PREFIX cc: <http://creativecommons.org/ns#> 
                PREFIX dcterms: <http://purl.org/dc/terms/> 
                PREFIX foaf: <http://xmlns.com/foaf/0.1/> 
                PREFIX gn: <http://www.geonames.org/ontology#> 
                PREFIX owl: <http://www.w3.org/2002/07/owl#>
                PREFIX dbpedia-owl:<http://dbpedia.org/ontology/>
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#> 
                PREFIX xml: <http://www.w3.org/XML/1998/namespace> 
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                PREFIX vrank: <http://purl.org/voc/vrank#>
                PREFIX wb: <http://data.worldbank.org/>
                PREFIX earth: <http://linked.earth/ontology#>
                PREFIX hto: <http://vcharpenay.github.io/hto/hto.xml#>
                PREFIX cro: <http://rhizomik.net/ontologies/copyrightonto.owl#>
            
            
                select ?name ?region ?income ?life ?gdp ?internetuser ?pop ?co2 ?ranking ?climate where {
							{?s <http://www.geonames.org/ontology#name>""" + name + """ .
							?s <http://www.geonames.org/ontology#neighbour> ?nei .
							?nei <http://www.geonames.org/ontology#name> ?name .
							?nei <http://data.worldbank.org/region> ?region .
							?nei <http://data.worldbank.org/incomeLevel> ?income .
							?nei <http://dbpedia.org/ontology/lifeExpectancy> ?life .
							?nei <http://dbpedia.org/ontology/grossDomesticProduct> ?gdp .
							?nei <http://rhizomik.net/ontologies/copyrightonto.owl#count> ?internetuser .
							?nei <http://www.geonames.org/ontology#population> ?pop .
							?nei <http://vcharpenay.github.io/hto/hto.xml#CO2> ?co2 .
							?nei <http://dbpedia.org/ontology/countryRank> ?ranking .
							?nei <http://dbpedia.org/ontology/climate> ?climate}
							UNION{
							?s <http://www.geonames.org/ontology#name>""" + name + """ .
                           ?s <http://www.geonames.org/ontology#name> ?name .
                           ?s <http://data.worldbank.org/region> ?region .
                           ?s <http://data.worldbank.org/incomeLevel> ?income .
                           ?s <http://dbpedia.org/ontology/lifeExpectancy> ?life .
                           ?s <http://dbpedia.org/ontology/grossDomesticProduct> ?gdp .
                           ?s <http://rhizomik.net/ontologies/copyrightonto.owl#count> ?internetuser .
                           ?s <http://www.geonames.org/ontology#population> ?pop .
                           ?s <http://vcharpenay.github.io/hto/hto.xml#CO2> ?co2 .
                           ?s <http://dbpedia.org/ontology/countryRank> ?ranking .
                           ?s <http://dbpedia.org/ontology/climate> ?climate
							
							}
							
							}
            """
            sparql.setQuery(rq)
            print("check neigbour")
            sparql.setReturnFormat(JSON)
            
            #sparql.addParameter('reasoning', 'true')
            
            data_json = sparql.query().convert()
            '''
            data_json["results"]["bindings"]: list of dictionary
            
            {income:}
            '''
            for result in data_json["results"]["bindings"]:
                countryObject={}
                for key, value in result.items():
                    #my_attribute = {}
                    #my_attribute[key] = value['value']
                    countryObject.update({key:value['value']})
                list_of_countries.append(countryObject)
            return list_of_countries
        
    if len(name)==0:
        if region!="All":
            print("check request endpint 1")
            #request_nach_region:
            region = '"' + region+'"'
            rq = """
            PREFIX cc: <http://creativecommons.org/ns#> 
            PREFIX dcterms: <http://purl.org/dc/terms/> 
            PREFIX foaf: <http://xmlns.com/foaf/0.1/> 
            PREFIX gn: <http://www.geonames.org/ontology#> 
            PREFIX owl: <http://www.w3.org/2002/07/owl#>
            PREFIX dbpedia-owl:<http://dbpedia.org/ontology/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
            PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#> 
            PREFIX xml: <http://www.w3.org/XML/1998/namespace> 
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            PREFIX vrank: <http://purl.org/voc/vrank#>
            PREFIX wb: <http://data.worldbank.org/>
            PREFIX earth: <http://linked.earth/ontology#>
            PREFIX hto: <http://vcharpenay.github.io/hto/hto.xml#>
            PREFIX cro: <http://rhizomik.net/ontologies/copyrightonto.owl#>
        
        
            select * where {?s <http://data.worldbank.org/region> """+region+""" .
                            ?s <http://data.worldbank.org/region> ?region .
                        ?s <http://www.geonames.org/ontology#name> ?name .
                       ?s <http://data.worldbank.org/incomeLevel> ?income .
                       ?s <http://dbpedia.org/ontology/lifeExpectancy> ?life .
                       ?s <http://dbpedia.org/ontology/grossDomesticProduct> ?gdp .
                       ?s <http://rhizomik.net/ontologies/copyrightonto.owl#count> ?internetuser .
                       ?s <http://www.geonames.org/ontology#population> ?pop .
                       ?s <http://vcharpenay.github.io/hto/hto.xml#CO2> ?co2 .
                       ?s <http://dbpedia.org/ontology/countryRank> ?ranking .
                       ?s <http://dbpedia.org/ontology/climate> ?climate
                       }
        """
            sparql.setQuery(rq)
            sparql.setReturnFormat(JSON)
            data_json = sparql.query().convert()
            '''
            data_json["results"]["bindings"]: list of dictionary
            
            {income:}
            '''
            print("check request endpoint")
            #list_of_countries=[]
            for result in data_json["results"]["bindings"]:
                print(result)
                countryObject={}
                for key, value in result.items():
                    countryObject.update({key:value['value']})
                list_of_countries.append(countryObject)
            print("check again")
            print(len(list_of_countries))
            #for countryObject in list_of_countries:
             #   print(countryObject['income'])
            
            
            if income!="All":
                print("input income: " + income)
                i=0
                for countryObject in list_of_countries:
                    i = i+1
                print(i)
                list_countries_temp = copy.deepcopy(list_of_countries)
                print([(countryObject["name"],countryObject["income"]) for countryObject in list_countries_temp])
                
                for countryObject in list_countries_temp:
                    if countryObject['income']!=income:
                        list_of_countries.remove(countryObject)
                    else:
                        print("h")
            if climate!="All":
                list_countries_temp = copy.deepcopy(list_of_countries)
                print([(countryObject["name"],countryObject["climate"]) for countryObject in list_countries_temp])
                
                for countryObject in list_countries_temp:
                    if countryObject['climate']!=climate:
                        list_of_countries.remove(countryObject)
                    else:
                        print("h")
                #list_countries_temp = list_of_countries
                #for countryObject in list_countries_temp:
                 #   print(countryObject)
                  #  if (countryObject['climate']!=climate):
                   #     print(countryObject)
                    #    getIndex = list_of_countries.index(countryObject)
                     #   list_of_countries.remove(countryObject)
            if len(list_of_countries) <= count:
                return list_of_countries
            else:
                return list_of_countries[:count]
        if region=="All":
            #request_nach_region:
            rq = """
            PREFIX cc: <http://creativecommons.org/ns#> 
            PREFIX dcterms: <http://purl.org/dc/terms/> 
            PREFIX foaf: <http://xmlns.com/foaf/0.1/> 
            PREFIX gn: <http://www.geonames.org/ontology#> 
            PREFIX owl: <http://www.w3.org/2002/07/owl#>
            PREFIX dbpedia-owl:<http://dbpedia.org/ontology/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
            PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#> 
            PREFIX xml: <http://www.w3.org/XML/1998/namespace> 
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            PREFIX vrank: <http://purl.org/voc/vrank#>
            PREFIX wb: <http://data.worldbank.org/>
            PREFIX earth: <http://linked.earth/ontology#>
            PREFIX hto: <http://vcharpenay.github.io/hto/hto.xml#>
            PREFIX cro: <http://rhizomik.net/ontologies/copyrightonto.owl#>
        
        
            select * where {
                        ?s <http://www.geonames.org/ontology#name> ?name .
                        ?s <http://data.worldbank.org/region> ?region .
                       ?s <http://data.worldbank.org/incomeLevel> ?income .
                       ?s <http://dbpedia.org/ontology/lifeExpectancy> ?life .
                       ?s <http://dbpedia.org/ontology/grossDomesticProduct> ?gdp .
                       ?s <http://rhizomik.net/ontologies/copyrightonto.owl#count> ?internetuser .
                       ?s <http://www.geonames.org/ontology#population> ?pop .
                       ?s <http://vcharpenay.github.io/hto/hto.xml#CO2> ?co2 .
                       ?s <http://dbpedia.org/ontology/countryRank> ?ranking .
                       ?s <http://dbpedia.org/ontology/climate> ?climate
                       }
        """
            sparql.setQuery(rq)
            sparql.setReturnFormat(JSON)
            data_json = sparql.query().convert()
            '''
            data_json["results"]["bindings"]: list of dictionary
            
            {income:}
            '''
            print("check request endpoint")
            #list_of_countries=[]
            for result in data_json["results"]["bindings"]:
                print(result)
                countryObject={}
                for key, value in result.items():
                    countryObject.update({key:value['value']})
                list_of_countries.append(countryObject)
            print("check again")
            print(len(list_of_countries))
            #for countryObject in list_of_countries:
             #   print(countryObject['income'])
            
            
            if income!="All":
                print("input income: " + income)
                i=0
                for countryObject in list_of_countries:
                    i = i+1
                print(i)
                list_countries_temp = copy.deepcopy(list_of_countries)
                print([(countryObject["name"],countryObject["income"]) for countryObject in list_countries_temp])
                
                for countryObject in list_countries_temp:
                    if countryObject['income']!=income:
                        list_of_countries.remove(countryObject)
                    else:
                        print("h")
            if climate!="All":
                list_countries_temp = copy.deepcopy(list_of_countries)
                print([(countryObject["name"],countryObject["climate"]) for countryObject in list_countries_temp])
                
                for countryObject in list_countries_temp:
                    if countryObject['climate']!=climate:
                        list_of_countries.remove(countryObject)
                    else:
                        print("h")
                #list_countries_temp = list_of_countries
                #for countryObject in list_countries_temp:
                 #   print(countryObject)
                  #  if (countryObject['climate']!=climate):
                   #     print(countryObject)
                    #    getIndex = list_of_countries.index(countryObject)
                     #   list_of_countries.remove(countryObject)
            if len(list_of_countries) <= count:
                return list_of_countries
            else:
                return list_of_countries[:count]
            
            
#            if climate!="All":
#                for countryObject in list_of_countries:
#                    if (countryObject['climate']!=climate):
#                        print(countryObject)
#                        getIndex = list_of_countries.index(countryObject)
#                        list_of_countries.remove(countryObject)
#            if income!="All":
#                for countryObject in list_of_countries:
#                    if (countryObject['income']!=income):
#                        print(countryObject)
#                        getIndex = list_of_countries.index(countryObject)
#                        list_of_countries.remove(countryObject)
#            if len(list_of_countries) <= count:
#                return list_of_countries
#            else:
#                return list_of_countries[:count]
            
#        if region=="All" and income=="All" and climate=="All":
#            limit = count
#            rq = """
#            PREFIX cc: <http://creativecommons.org/ns#> 
#            PREFIX dcterms: <http://purl.org/dc/terms/> 
#            PREFIX foaf: <http://xmlns.com/foaf/0.1/> 
#            PREFIX gn: <http://www.geonames.org/ontology#> 
#            PREFIX owl: <http://www.w3.org/2002/07/owl#>
#            PREFIX dbpedia-owl:<http://dbpedia.org/ontology/>
#            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
#            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
#            PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#> 
#            PREFIX xml: <http://www.w3.org/XML/1998/namespace> 
#            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
#            PREFIX vrank: <http://purl.org/voc/vrank#>
#            PREFIX wb: <http://data.worldbank.org/>
#            PREFIX earth: <http://linked.earth/ontology#>
#            PREFIX hto: <http://vcharpenay.github.io/hto/hto.xml#>
#            PREFIX cro: <http://rhizomik.net/ontologies/copyrightonto.owl#>
#        
#        
#            select * where {?s <http://www.geonames.org/ontology#name> ?name .
#                       ?s <http://data.worldbank.org/region> ?region .
#                       ?s <http://data.worldbank.org/incomeLevel> ?income .
#                       ?s <http://dbpedia.org/ontology/lifeExpectancy> ?life .
#                       ?s <http://dbpedia.org/ontology/grossDomesticProduct> ?gdp .
#                       ?s <http://rhizomik.net/ontologies/copyrightonto.owl#count> ?internetuser .
#                       ?s <http://www.geonames.org/ontology#population> ?pop .
#                       ?s <http://vcharpenay.github.io/hto/hto.xml#CO2> ?co2 .
#                       ?s <http://dbpedia.org/ontology/countryRank> ?ranking .
#                       ?s <http://dbpedia.org/ontology/climate> ?climate
#                       }
#            limit """+limit+"""
#        """
#            sparql.setQuery(rq)
#            sparql.setReturnFormat(JSON)
#            
#            #sparql.addParameter('reasoning', 'true')
#            
#            data_json = sparql.query().convert()
#            '''
#            data_json["results"]["bindings"]: list of dictionary
#            
#            {income:}
#            '''
#            for result in data_json["results"]["bindings"]:
#                countryObject={}
#                for key, value in result.items():
#                    #my_attribute = {}
#                    #my_attribute[key] = value['value']
#                    countryObject.update({key:value['value']})
#                search_results.append(countryObject)
#            
#                
#    return search_results            