# -*- coding: utf-8 -*-
"""
Created on Fri Aug  2 10:36:00 2019

@author: GPRA489
"""

from app import db #from module app import my Object db
#db ist object of database object.Columm... 
 
class Artist(db.Model):
    __tablename__ = "artists"
 
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
 
    def __repr__(self):
        return "<Artist: {}>".format(self.name)
 
 
class Album(db.Model):
    """"""
    __tablename__ = "albums"
 
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    release_date = db.Column(db.String)
    publisher = db.Column(db.String)
    media_type = db.Column(db.String)
 
    artist_id = db.Column(db.Integer, db.ForeignKey("artists.id"))
    artist = db.relationship("Artist", backref=db.backref(
        "albums", order_by=id), lazy=True)
    