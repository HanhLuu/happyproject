# -*- coding: utf-8 -*-
"""
Created on Mon Aug  5 08:21:03 2019

@author: GPRA489
"""

from flask_table import Table, Col
 
class Results(Table):
    id = Col('Id', show=False)
    region = Col('Region')
    income = Col('Income')
    climate = Col('Climate')
