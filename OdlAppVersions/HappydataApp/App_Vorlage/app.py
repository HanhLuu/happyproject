# -*- coding: utf-8 -*-
"""
Created on Fri Aug  2 10:32:24 2019

@author: GPRA489
"""

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
 
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///mymusic.db' #tell app where database file lives
app.secret_key = "flask rocks!"
'''
db_creator we have created a mymusic.db
db ist new Object of mymusic.db integrated into Flask
''' 
db = SQLAlchemy(app) #Object db allows us to integrate SQLAlchemy into Flask