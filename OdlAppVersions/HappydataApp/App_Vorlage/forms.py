# -*- coding: utf-8 -*-
"""
Created on Fri Aug  2 11:03:22 2019

@author: GPRA489
"""

# forms.py

'''
Search Form for filtering our music database's result
'''
 
from wtforms import Form, StringField, SelectField

class CountrySearchForm(Form):
	choices = [('Country', 'Country'),
               ('Album', 'Album'),
               ('Publisher', 'Publisher')]
	choices_re = [('EAP', 'East Asia & Pacific'),
               ('ECA', 'Europe & Central Asia'),
			   ('MENA', 'Middle East & North Africa'),
			   ('NA', 'North America'),
			   ('SA', 'South Asia'),
			   ('SSA', 'Sub-Saharan Africa'),
               ('LAC', 'Latin America & Caribbean')]
	choices_in = [('HI', 'High income'),
               ('LAMI', 'Low & middle income'),
			   ('LI', 'Low income'),
			   ('LMI', 'Lower middle income'),
			   ('MI', 'Middle income'),
               ('UMI', 'Upper middle income')]
	choices_cl = [('TZ', 'Temperate zone'),
               ('MSZ', 'Mediterranean/subtropical zone'),
			   ('HDZ', 'Hot dry zone'),
			   ('HHTZ', 'Hot humid/tropical zone'),
               ('HHH', 'Hot/higher humidity')]
	select_re = SelectField('', choices=choices_re)
	select_in = SelectField('', choices=choices_in)
	select_cl = SelectField('', choices=choices_cl)
	search_name = StringField('')
	search_count = StringField('')



'''
defines all the fields we need to creat a new Album
AlbumForm to make new instance for Album and then adding to database 
'''


	