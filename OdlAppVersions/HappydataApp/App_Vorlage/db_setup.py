# -*- coding: utf-8 -*-
"""
Created on Fri Aug  2 10:41:32 2019

@author: GPRA489
"""

# db_setup.py
 
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
 
engine = create_engine('sqlite:///mymusic.db', convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
'''
the scoped_session() function is provided which produces 
a thread-managed registry of Session objects. 
It is commonly used in web applications so that a single global variable 
can be used to safely represent transactional sessions with sets of objects, 
localized to a single thread.
'''
Base = declarative_base()
'''
Return a class property which produces
a Query object against the class and the current Session when called.
'''
Base.query = db_session.query_property()
 
def init_db():
    import models
    Base.metadata.create_all(bind=engine)